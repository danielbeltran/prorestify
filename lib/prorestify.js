'use strict';

const _ = require('lodash');
const winston = require('winston');
const BPromise = require('bluebird');
// trick to colorize logs
winston.remove(winston.transports.Console);
winston.add(winston.transports.Console, {colorize: true});
const fs = require('fs');
const callsite = require('callsite');
const path = require('path');
const joi = require('joi');
const requireDir = require('require-dir');
const flatten = require('flat');
const inflector = require('inflected');
const changeCase = require('change-case');

function ProRESTify(options) {
  let self = this;
  let stack = callsite();
  let callerPath = path.dirname(stack[1].getFileName());

  if (!(this instanceof ProRESTify)) {
    return new ProRESTify(options);
  }

  // validate the options
  let validationResult = joi.validate(options, self.optionsValidationSchema);
  if (validationResult.error === null) {
    // merge options with default values
    self.options = _.merge({}, self.defaults, options);
    // where we will store the endpoints
    self.endpoints = [];

    // preppend caller path to the route folder if the default value is beeing used
    if (options === undefined || options.routeFolder === undefined) {
      self.options.routeFolder = callerPath + self.options.routeFolder;
    }

    // advanced validation for the path. Make sure the path exists
    if (!fs.existsSync(self.options.routeFolder)) {
      throw 'ProRESTify - the folder: ' + self.options.routeFolder + ' does not exist';
    }
  } else {
    throw validationResult.error;
  }
}

// default options' values
ProRESTify.prototype.defaults = {
  routeFolder: '/routes/',
  allowUnknown: false,
  abortEarly: false,
  idRegExp: /[0-9]+|[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}/, // Integer or uuid
  validate: BPromise.promisify(joi.validate)
};

// validation schema for the options
ProRESTify.prototype.optionsValidationSchema = joi.object().keys({
  routeFolder: joi.string(),
  allowUnknown: joi.boolean().strict(),
  abortEarly: joi.boolean().strict(),
  idRegExp: joi.object().type(RegExp),
  validate: joi.func().minArity(2).maxArity(3)
});

// retrieve endpoints based on the folder and file structure
ProRESTify.prototype.retrieveEndpoints = function(app) {
  let self = this;
  let routeDir = requireDir(self.options.routeFolder, {recurse: true, duplicates: true});
  
  // empty existing endpoints first
  self.endpoints = [];

  // if there is no endpoint yet
  if (!_.isEmpty(routeDir)) {
    let flatDir = flatten(routeDir, {delimiter: '_'});

    _.forEach(flatDir, (val, key) => {
      // only process if it contains .js (meaning this is a file)
      if (self.matchAction(key)) {
        // verify if version exists
        if (self.matchVersion(key)) {
          let endpoints = self.getEndpointsData(app, key);
          endpoints.forEach(endpoint => {
            self.endpoints.push(endpoint);
          });
        } else {
          //remove what's after the .js
          let parts = key.split('.');
          let file = parts[0];
          let action = parts[1].split('_')[1];
          winston.warn('ProRESTify - version is missing for file:' + self.options.routeFolder + self.replaceAll(file, '_', '/') + '.js action:' + action + ' cannot retrieve the endpoint');
          // we skip this endpoint but we continue with the rest of the process
        }
      } else if(self.matchFile(key)) {
        winston.warn('ProRESTify - No endpoints were found in this existing file:' + self.options.routeFolder + self.replaceAll(key, '_', '/'));
        // we skip this endpoint but we continue with the rest of the process
      }
    });
  } else {
    winston.warn('ProRESTify - folder:' + self.options.routeFolder + ' is empty. No endpoints detected');
    // nothing else to do then
  }
};

ProRESTify.prototype.replaceAll = function(string, search, replacement) {
  return string.replace(new RegExp(search, 'g'), replacement);
};

ProRESTify.prototype.matchVersion = function(string) {
  return string.match(/^v\d*/);
};

ProRESTify.prototype.matchAction = function(string) {
  return string.match(/\.js_/);
};

ProRESTify.prototype.matchFile = function(string) {
  return string.match(/\.js$/);
};

ProRESTify.prototype.matchGetAction = function(string) {
  return string.match(/^get.*/);
};

ProRESTify.prototype.generateParamId = function(param) {
  let self = this;
  return '/:' + inflector.singularize(changeCase.camelCase(param)) + 'Id' + '(' + self.options.idRegExp.toString().slice(1, -1) + ')';
};

ProRESTify.prototype.formatActionUrl = function(string) {
  let self = this;

  if (self.matchGetAction(string)) {
    string = string.substring(3);
  }
  return changeCase.paramCase(string);
};

// retrieve endpoint data and build URL with param like :userId in it
ProRESTify.prototype.getEndpointsData = function(app, flattenUrl) {
  let self = this;

  // separate url and action
  let urlAction = flattenUrl.split('.');
  // get url parts
  let parts = urlAction[0].split('_');
  // get action
  let action = urlAction[1].split('_')[1];
  let isCustomAction = false;

  // build url part
  parts = _.map(parts, (part, i) => {
    if (!self.matchVersion(part) && i !== (parts.length -1)) {
      return part += self.generateParamId(part);
    }
    return changeCase.paramCase(part);
  });

  let baseUrl = '/' + parts.join('/');

  // recover the file
  let fileParts = flattenUrl.split('.js_');
  let actionFile = fileParts[1];
  let filePath = self.options.routeFolder + self.replaceAll(fileParts[0], '_', '/') + '.js';
  let file = require(filePath);
  let typeAction = file[actionFile](app);

  // with the action system we might have 2 endpoints
  let endpoints = [{
    action: action,
    url: baseUrl,
    isCustomAction: false,
    validation: typeAction.validation,
    middlewares: typeAction.middlewares,
    controller: typeAction.controller
  }];

  // build action part
  switch (action) {
  case 'create':
    endpoints[0].method = 'POST';
    break;
  case 'list':
    endpoints[0].method = 'GET';
    break;
  case 'get':
    endpoints[0].method = 'GET';
    endpoints[0].url += self.generateParamId(parts[parts.length - 1]);
    break;
  case 'update':
    endpoints[0].method = 'PUT';
    endpoints[0].url += self.generateParamId(parts[parts.length - 1]);
    break;
  case 'patch':
    endpoints[0].method = 'PATCH';
    endpoints[0].url += self.generateParamId(parts[parts.length - 1]);
    break;
  case 'del':
    endpoints[0].method = 'DELETE';
    endpoints[0].url += self.generateParamId(parts[parts.length - 1]);
    break;

  default:
    isCustomAction = true;
    let httpMethod;
    if (self.matchGetAction(action)) {
      httpMethod = 'GET';
    } else {
      httpMethod = 'POST';
    }
    // empty exiting data
    endpoints = [];

    _.forEach(typeAction, (func, type) => {
      switch(type) {
      case 'item':
        let tmpUrl = baseUrl + self.generateParamId(parts[parts.length - 1]);
        endpoints.push({
          url: tmpUrl + '/' + self.formatActionUrl(actionFile),
          method: httpMethod,
          isCustomAction: true,
          validation: func.validation,
          middlewares: func.middlewares,
          controller: func.controller
        });
        break;
      case 'collection':
        endpoints.push({
          url: baseUrl + '/' + self.formatActionUrl(actionFile),
          method: httpMethod,
          isCustomAction: true,
          validation: func.validation,
          middlewares: func.middlewares,
          controller: func.controller
        });
        break;
      default:
        winston.warn('ProRESTify - Unknown action type for file:' + filePath + '. No endpoint was created');
        // nothing else to do then
        break;
      }
    });
    break;
  }

  return endpoints;
};

ProRESTify.prototype.hookHandler = function(hookFunction) {
  let self = this;
  return (req, res, next) => {
    return hookFunction(req, res)
      .then(() => {
        next();
      })
      .catch(err => {
        self.hookFailure(err);
        next(err);
      });
  };
};

// meant to be overrided
ProRESTify.prototype.hookBeforeValidation = function() {
  return BPromise.resolve();
};

// meant to be overrided
ProRESTify.prototype.hookBeforeMiddlewares = function() {
  return BPromise.resolve();
};

// meant to be overrided
ProRESTify.prototype.hookBeforeController = function() {
  return BPromise.resolve();
};

// meant to be overrided
ProRESTify.prototype.hookSuccess = function() {
  return BPromise.resolve();
};

// meant to be overrided
ProRESTify.prototype.hookFailure = function() {
  return BPromise.resolve();
};

// handle validation if exists
ProRESTify.prototype.validationHandler = function(validation) {
  let self = this;
  return (req, res, next) => {
    return BPromise.resolve()
      .then(() => {
        let toValidate = req.method === 'GET' ? req.query : req.body;
        return self.options.validate(toValidate, validation || {}, {
          abortEarly: self.options.abortEarly,
          allowUnknown: self.options.allowUnknown
        });
      })
      .then((data) => {
        req.validatedData = data;
        next();
      })
      .catch(err => {
        self.hookFailure(req, err);
        next(err);
      });
  };
};

// handle middlewares if exists
ProRESTify.prototype.middlewaresHandler = function(middlewares) {
  let self = this;

  return (req, res, next) => {
    return BPromise.each(middlewares || [], (middleware) => {
      return middleware(req, res);
    })
    .then(() => {
      // call next without param in case the last middleware returns something
      next();
    })
    .catch(err => {
      self.hookFailure(req, err);
      next(err);
    });
  };
};

ProRESTify.prototype.controllerHandler = function(action, controller) {
  let self = this;

  return (req, res, next) => {
    return BPromise.resolve() // make the flow a promise
      .then(() => {
        return controller(req.validatedData, req.params, res);
      })
      .then((response) => {
        let statusCode = 200;
        if (response === undefined) {
          statusCode = 204;
        }
        if (action === 'create') {
          statusCode = 201;
        }

        self.hookSuccess(req, statusCode);
        // TODO: Send corresponding http status code
        res.status(statusCode).json(response);
      })
      .catch(err => {
        self.hookFailure(req, err);
        next(err);
      });
  };
};

// Assign all endpoint to express
ProRESTify.prototype.router = function(app) {
  let self = this;

  // retrieve endpoints
  self.retrieveEndpoints(app);

  function sortByCustomAction(a, b) {
    if (a.isCustomAction && !b.isCustomAction) {
      return -1;
    }
    if (!a.isCustomAction && b.isCustomAction) {
      return 1;
    }
    return 0;
  }

  // add each endpoints to express (custom actions first to avoid collisions)
  self.endpoints.sort(sortByCustomAction).forEach(endpoint => {
    let functionName = endpoint.method.toLowerCase();
    app[functionName](
      endpoint.url,
      self.hookHandler(self.hookBeforeMiddlewares),
      self.middlewaresHandler(endpoint.middlewares),
      self.hookHandler(self.hookBeforeValidation),
      self.validationHandler(endpoint.validation),
      self.hookHandler(self.hookBeforeController),
      self.controllerHandler(endpoint.action, endpoint.controller)
    );
    winston.info('Endpoint ' + endpoint.method + '=>' + endpoint.url + ' successfully loaded');
  });
};

module.exports = ProRESTify;
